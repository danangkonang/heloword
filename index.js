const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.status(200).json({
    ping: 'pong'
  })
});

app.listen(8989, function() {
  console.log('App running on http://localhost:' + 8989);
});
