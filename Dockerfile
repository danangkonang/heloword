FROM node:lts-alpine3.16

WORKDIR /app

COPY . .

RUN yarn install

ENV NODE_ENV=production


EXPOSE 8989

CMD ["node", "index"]
